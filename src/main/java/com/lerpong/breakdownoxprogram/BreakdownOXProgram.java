/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lerpong.breakdownoxprogram;

import java.util.Scanner;

public class BreakdownOXProgram {

    static char winner = '-';
    static boolean isFinish = false;
    static Scanner k = new Scanner(System.in);
    static int row, col;
    static char player = 'X';
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

    private static void showWelcome() {
        System.out.println("Wlcome to OX Game");
    }

    private static void showTable() {
        System.out.print(" 123");
        System.out.println("");
        for (row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    private static void showTurn() {
        System.out.println(player + " turn");
    }

    private static void input() {
        while (true) {
            try {
                System.out.println("Please input Row Col:");
                row = k.nextInt() - 1;
                col = k.nextInt() - 1;
                if (table[row][col] == '-') {
                    table[row][col] = player;
                    break;
                }
            } catch (Exception e) {
                showTable();
                System.out.println("Error: table at row and col is not empty!!!");
            }
        }
    }

    private static void checkCol() {
        for (row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    private static void checkRow() {
        for (col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    private static void checkCross() {
        if(table[2][2] != '-'){
            int n =0;
            for(row=0,col=0; col<2; col++,row++){
                if(table[row][col]==table[row+1][col+1]){
                    n++;
                    if(n==2){
                        winner = player;
                        isFinish = true;
                        return;
                    }
                }
            }
            n =0;
            for(row=2,col=0; col<2; col++,row--){
                if(table[row][col]==table[row-1][col+1]){
                    n++;
                    if(n==2){
                        winner = player;
                        isFinish = true;
                        return;
                    }
                }
            }
        }
    }

    private static void checkDraw() {
        for (row = 0; row < table.length; row++) {
            for (col = 0; col < table[row].length; col++) {
                if (table[row][col] == '-') {
                    return;
                }
            }
        }
        isFinish = true;
    }

    private static void checkWin() {
        checkRow();
        checkCol();
        checkCross();
        checkDraw();
    }
    
    private static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    private static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!!");
        } else {
            System.out.println(winner + " win!!!");
        }
        showTable();
    }

    private static void showBye() {
        System.out.println("Bye bye ....");
    }

}